/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://42.115.221.44:8080/crud-api/users/";
const gUSER_ID_COL = 0;
const gFIRST_NAME_COL = 1;
const gLAST_NAME_COL = 2;
const gCOUNTRY_COL = 3;
const gSUBJECT_COL = 4;
const gCUSTOMER_TYPE_COL = 5;
const gREGISTER_STATUS_COL = 6;
const gACTION_COL = 7;
var gNameCol = ['id', 'firstname', 'lastname', 'country', 'subject', 'customerType', 'registerStatus', 'action'];
var gUserId = 0;

$(document).ready(function () {
  onPageLoading();

  //Edit button
  $("#user-table").on('click', '.btn-edit', function () {
    onBtnActionClick(this, 'edit');
  });

  //Delete button
  $("#user-table").on('click', '.btn-delete', function () {
    onBtnActionClick(this, 'delete');
  });

  //Add new user button
  $("#btn-add-user").on('click', function () {
    onBtnAddNewUserClick();
  });

  // Add new user modal
  $("#btn-modal-insert-user").on('click', function () {
    onBtnInsertUserModalClick();
  });

  // Edit user modal
  $("#btn-modal-update-user").on('click', function () {
    onBtnUpdateUserModalClick();
  });

  // Delete user modal
  $("#btn-modal-delete-user").on('click', function () {
    onBtnDeleteUserModalClick();
  });
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
function onPageLoading() {
  "use-strict"
  callApiGetAllUsers();
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function callApiGetAllUsers() {
  "use-strict"
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: 'json',
    async: false,
    success: renderDaTaToTable,
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function renderDaTaToTable(paramResponse) {
  "use-strict"
  console.log("ghi được Response ra console");
  console.log(paramResponse);

  var vTable = $("#user-table").DataTable({
    columns: [
      { data: gNameCol[gUSER_ID_COL] },
      { data: gNameCol[gFIRST_NAME_COL] },
      { data: gNameCol[gLAST_NAME_COL] },
      { data: gNameCol[gCOUNTRY_COL] },
      { data: gNameCol[gSUBJECT_COL] },
      { data: gNameCol[gCUSTOMER_TYPE_COL] },
      { data: gNameCol[gREGISTER_STATUS_COL] },
      { data: gNameCol[gACTION_COL] },
    ],
    columnDefs: [
      {
        targets: gACTION_COL,
        className: "text-center",
        defaultContent: `
            <div class="btn-group" role="group">
              <button class="btn btn-primary rounded btn-edit mr-2">Sửa</button>
              <button class="btn btn-danger rounded btn-delete">Xoá</button>
            </div>
            `
      }
    ],
    bDestroy: true // fix error cannot reinitialise data table
  });
  addDataToTable(paramResponse);
}

function addDataToTable(paramResponse) {
  "use-strict"
  var vTable = $("#user-table").DataTable();
  vTable.clear();
  vTable.rows.add(paramResponse);
  vTable.draw();
}

function onBtnActionClick(paramBtn, paramAction) {
  "use-strict"
  console.log("Nút " + paramAction + " được nhấn");
  var vThisRow = $(paramBtn).closest('tr');
  var vTable = $("#user-table").DataTable();
  var vThisRowData = vTable.row(vThisRow).data();
  console.log(vThisRowData);
  if (paramAction == 'edit') {
    //Get current user data and put to edit modal
    $("#firstname-edit").val(vThisRowData.firstname);
    $("#lastname-edit").val(vThisRowData.lastname);
    $("#subject-edit").val(vThisRowData.subject);
    $("#country-edit").val(vThisRowData.country);
    $("#customerType").val(vThisRowData.customerType);
    $("#registerStatus").val(vThisRowData.registerStatus);
    gUserId = vThisRowData.id; // Gán userId cho biến để sử dụng cho hàm update
    //Hiện edit modal lên
    $("#edit-user-modal").modal('show');
  }
  if (paramAction == 'delete') {
    gUserId = vThisRowData.id;
    $("#delete-user-modal").modal('show');
  }
}

function onBtnAddNewUserClick() {
  "use-strict"
  $("#add-user-modal").modal('show');
}

function onBtnInsertUserModalClick() {
  "use-strict"
  // Khai báo đối tượng chứa dữ liệu
  var vUserObj = {
    firstname: "",
    lastname: "",
    subject: "",
    country: ""
  }
  console.log("Nút Insert user dc nhấn");
  //B1 thu thập dữ liệu
  getDataFromModal(vUserObj);
  //B2 xác nhận dữ liệu
  var vCheck = validateDataInPut(vUserObj);
  //B3 insert user
  if (vCheck) {
    callApiCreateNewUser(vUserObj);
  }
  //B4 xử lý front end
}

function getDataFromModal(paramUserObj) {
  "use-strict"
  paramUserObj.firstname = $("#firstname").val().trim();
  paramUserObj.lastname = $("#lastname").val().trim();
  paramUserObj.subject = $("#subject").val().trim();
  paramUserObj.country = $("#country").val();
}

function validateDataInPut(paramUserObj) {
  "use-strict"
  var vResult = true;
  if (paramUserObj.firstname == "") {
    alert("Chưa nhập firstname!");
    vResult = false;
  }
  if (paramUserObj.lastname == "") {
    alert("Chưa nhập lastname!");
    vResult = false;
  }
  if (paramUserObj.subject == "") {
    alert("Chưa nhập subject!");
    vResult = false;
  }
  if (paramUserObj.country == "") {
    alert("Chưa chọn country!");
    vResult = false;
  }
  return vResult;
}

function callApiCreateNewUser(paramUserObj) {
  "use-strict"
  $.ajax({
    url: gBASE_URL,
    type: "POST",
    dataType: 'json',
    data: JSON.stringify(paramUserObj),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: insertUserSuccess,
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function insertUserSuccess() {
  "use-strict"
  alert("Thêm user thành công");
  // xóa trắng dữ liệu trên modal
  $("#firstname").val('');
  $("#lastname").val('');
  $("#subject").val('');

  // tắt Modal
  $("#add-user-modal").modal('hide');

  //load lại bảng danh sách users
  callApiGetAllUsers();
}

function onBtnUpdateUserModalClick() {
  console.log("Nút update user dc nhấn!");
  "use-strict"
  // Khai báo đối tượng chứa dữ liệu
  var vUserObj = {
    firstname: "",
    lastname: "",
    subject: "",
    country: "",
    customerType: "",
    registerStatus: ""
  }

  //B1: Lấy dữ liệu
  vUserObj.firstname = $("#firstname-edit").val();
  vUserObj.lastname = $("#lastname-edit").val();
  vUserObj.subject = $("#subject-edit").val();
  vUserObj.country = $("#country-edit").val();
  vUserObj.customerType = $("#customerType").val();
  vUserObj.registerStatus = $("#registerStatus").val();

  //B2: Kiểm tra dữ liệu
  var vCheck = validateDataInPut(vUserObj); // Sử dụng chung hàm validate dành cho crate, update 

  //B3: Update user
  if (vCheck) {
    callApiUpdateUser(vUserObj);
  }
}

function callApiUpdateUser(paramUserObj) {
  "use-strict"
  $.ajax({
    url: gBASE_URL + gUserId,
    type: "PUT",
    dataType: 'json',
    data: JSON.stringify(paramUserObj),
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: updateUserSuccess,
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function updateUserSuccess(paramUserObj) {
  "use-strict"
  alert("Update user: " + paramUserObj.id + " thành công");

  // xóa trắng dữ liệu trên modal
  $("#firstname-edit").val();
  $("#lastname-edit").val();
  $("#subject-edit").val();

  // tắt Modal
  $("#edit-user-modal").modal('hide');

  //load lại bảng danh sách users
  callApiGetAllUsers();
}

function onBtnDeleteUserModalClick() {
  "use-strict"
  $.ajax({
    url: gBASE_URL + gUserId,
    type: "DELETE",
    contentType: "application/json;charset=UTF-8",
    async: false,
    success: deleteUserSuccess,
    error: function (e) {
      console.assert(e.responseText);
    }
  });
}

function deleteUserSuccess() {
  "use-strict"
  alert("Delete user: " + gUserId + " thành công");

  // tắt Modal
  $("#delete-user-modal").modal('hide');

  //load lại bảng danh sách users
  callApiGetAllUsers();
}